import React from 'react';
import { StyleSheet, Text, View, Platform } from 'react-native';
import MapView, {Marker, AnimatedRegion, Polyline} from 'react-native-maps';


//STYLES
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
}   ,
    map: {
        ...StyleSheet.absoluteFillObject,
    }
});


//Component
const Map = () => ({
    // 
    render() {
        const { region } = this.props;
        console.log(region);
    
    return(
        <View style={styles.container}>
        <MapView
        initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }}
        />
    </View>
    )
    }
    
});




export default Map;