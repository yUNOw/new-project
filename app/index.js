import { Navigation } from 'react-native-navigation';

import Screen1 from './screens/Screen1';
import Screen2 from './screens/Screen2';
//import Screen3 from './screens/Screen3';

export default () => {
    Navigation.registerComponent('Maps', () => Screen1);
    Navigation.registerComponent('User', () => Screen2);
    //Navigation.registerComponent('Screen3', () => Screen3);

    
    Navigation.startTabBasedApp({
        tabs:[
            {
                label: 'Maps',
                screen: 'Maps',
                icon: require('./images/map-icon.png'),
                selectedIcon: require('./images/map-icon-selected.png'),
                title: 'Map'
            },
            {
                label: 'User',
                screen: 'User',
                icon: require('./images/user-icon.png'),
                selectedIcon: require('./images/user-icon-selected.png'),
                title: 'User Info'
            }
        ]
    })
}