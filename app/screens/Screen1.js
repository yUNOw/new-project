import React, { Component } from 'react';
import { Text } from 'react-native';
import Container from '../components/Container';
import Maps from '../components/Maps';


class Screen extends Component {
  handlePress = () => {
    this.props.navigator.push({
      screen: 'Maps',
      title: 'Map',
    });
  };

  render() {
    return (
      // <Container
      //   backgroundColor="#F44336"
      //   onPress={this.handlePress}
      // />
      <Maps/>
    );
  }
}



export default Screen;